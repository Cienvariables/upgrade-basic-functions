// Iteración #1: Buscar el máximo
// Completa la función que tomando dos números como argumento devuelva el más alto.


const devuelveElMasAlto = (numberOne, numberTwo) => {
    let x = numberOne > numberTwo ? numberOne : numberTwo;
    return x;
};

var mas = devuelveElMasAlto(43, 342);

console.log(mas);

// Iteración #2: Buscar la palabra más larga
// Completa la función que tomando un array de strings como argumento
// devuelva el más largo, en caso de que dos strings tenga la misma
// longitud deberá devolver el primero.
// Puedes usar este array para probar tu función:
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];

let findLongestWord = avengers.reduce((acc, val) => acc.length > val.length ? acc : val, '');{
}

var resultado = findLongestWord;
console.log(resultado);

// Iteración #3: Calcular la suma
// Calcular una suma puede ser tan simple como iterar sobre un array y sumar cada uno de los elementos.
// Implemente la función denominada sumNumbers que toma un array de números como argumento
// y devuelve la suma de todos los números de la matriz.

// const numbers = [1, 2, 3, 5, 45, 37, 58];
// let total=0;
// const sumAll = (items) => {
// for (let i = 0; i < numbers.length; i++) {
//     total += numbers[i];
// }
// }
// let total1= sumAll(numbers);
// console.log(total);

// Iteración #4: Calcular el promedio
// Calcular un promedio es una tarea extremadamente común.
// Puedes usar este array para probar tu función:
const arrayNumeros = [12, 21, 38, 5, 45, 37, 6];

let total = 0;
let promedio = 0;

const average = (items) => {
    for (let x = 0; x < arrayNumeros.length; x++) {
        total += arrayNumeros[x], promedio = total / 2;
    }
}
let ejecutar = average(arrayNumeros);

console.log(promedio);

// Iteración #5: Calcular promedio de strings
// Crea una función que reciba por parámetro un array
// y cuando es un valor number lo sume y de lo contrario
// cuente la longitud del string y lo sume.
// Puedes usar este array para probar tu función:
const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];

function averageWord(array) {
    let suma4 = 0;
    for (c = 0; c < array.length; c++) {
        const element = array[c];
        if (typeof element === 'number') {
            suma4 += element;
        }
        else if (typeof element === 'string'){
            let longitud3 = element.length;
            suma4 += longitud3;
        }
    }
    return suma4;
}

let resultado1= averageWord(mixedElements);
console.log(resultado1);

// Iteración #6: Valores únicos
// Crea una función que reciba por parámetro un array y
// compruebe si existen elementos duplicados, en caso que
// existan los elimina para retornar un array sin los elementos
// duplicados. Puedes usar este array para probar tu función:

const duplicates = ['sushi', 'pizza', 'burger', 'potatoe', 'pasta',
    'ice-cream', 'pizza', 'chicken', 'onion rings', 'pasta', 'soda'];

function removeDuplicates(list) {
    let newduplicates = new Set([... new Set(duplicates)]);
}

let resultado5 = removeDuplicates;
console.log([... new Set(duplicates)]);

